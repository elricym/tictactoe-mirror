Tic Tac Toe

Author: Ming Yan 
Date: 10/1/2014

1.	INTRODUCTION
This game bases on the GroupCast protocol. It provides the functions that let two players play with each other online. The two players connect to each other through GroupCast on server, and interact using GroupCast commands. 

2.	PROTOCAL
1)	Form a group
We form groups for each of games. The maximum capacity of the groups is 2, which means a game can only accept 2 players. Once a client connects to the server, it will enter a group automatically if this group has only one member. If all of the groups are full or there isn��t any existing groups, it will form a new group and it will be the game holder.

2)	Confirming 
The two players have to know the names of the other one. Once a new group is founded, the holder will be waiting for the other. If a player enters this group, it will send a message of querying usernames of the group to server. Once this player gets the holder��s name, it will send a Game- Start message to the holder. Then the game will start finally if the holder receives this message.

3)	Playing
The holder moves first. Once any of the players make a move, it will send a message in this form: ��MSG,<destination>,x|y�� to the other one. X,y is the position of the move. The opponent will set an ��O�� on this position with this message. And the player will set a ��X�� after this move.
