package com.example.tictactoe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class TTTActivity extends Activity {

	// TAG for logging
	private static final String TAG = "TTTActivity";

	// server to connect to
	protected static final int GROUPCAST_PORT = 20000;
	protected static final String GROUPCAST_SERVER = "eece261.hopto.org";

	// networking
	Socket socket = null;
	BufferedReader in = null;
	PrintWriter out = null;
	boolean connected = false;

	// UI elements
	Button board[][] = new Button[3][3];
	Button bConnect = null;
	EditText etName = null;

	// group name
	String groupName;

	// user name
	String userName;
	String opponentName;

	// group numbers
	int groupNum;

	boolean holder = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_ttt);

		groupNum = 0;
		// find UI elements defined in xml
		bConnect = (Button) this.findViewById(R.id.bConnect);
		etName = (EditText) this.findViewById(R.id.etName);
		board[0][0] = (Button) this.findViewById(R.id.b00);
		board[0][1] = (Button) this.findViewById(R.id.b01);
		board[0][2] = (Button) this.findViewById(R.id.b02);
		board[1][0] = (Button) this.findViewById(R.id.b10);
		board[1][1] = (Button) this.findViewById(R.id.b11);
		board[1][2] = (Button) this.findViewById(R.id.b12);
		board[2][0] = (Button) this.findViewById(R.id.b20);
		board[2][1] = (Button) this.findViewById(R.id.b21);
		board[2][2] = (Button) this.findViewById(R.id.b22);

		// hide login controls
		hideLoginControls();

		// make the board non-clickable
		disableBoardClick();

		// hide the board
		hideBoard();

		// assign OnClickListener to connect button
		bConnect.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String name = etName.getText().toString();
				// sanitity check: make sure that the name does not start with
				// an @ character
				if (name == null || name.startsWith("@")) {
					Toast.makeText(getApplicationContext(), "Invalid name", Toast.LENGTH_SHORT).show();
				} else {
					send("NAME," + etName.getText());
				}
			}
		});

		// assign a common OnClickListener to all board buttons
		View.OnClickListener boardClickListener = new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				int x, y;
				switch (v.getId()) {
				case R.id.b00:
					x = 0;
					y = 0;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b01:
					x = 0;
					y = 1;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b02:
					x = 0;
					y = 2;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b10:
					x = 1;
					y = 0;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b11:
					x = 1;
					y = 1;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b12:
					x = 1;
					y = 2;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b20:
					x = 2;
					y = 0;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b21:
					x = 2;
					y = 1;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;
				case R.id.b22:
					x = 2;
					y = 2;
					board[x][y].setText("X");
					send("MSG," + opponentName + "," + x + "|" + y);
					disableBoardClick();
					break;

				// [ ... and so on for the other buttons ]

				default:
					break;
				}

			}
		};

		// assign OnClickListeners to board buttons
		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				board[x][y].setOnClickListener(boardClickListener);

		// start the AsyncTask that connects to the server
		// and listens to whatever the server is sending to us
		connect();

	}

	@Override
	protected void onDestroy() {
		Log.i(TAG, "onDestroy called");
		send("QUIT," + groupName);
		send("BYE");
		disconnect();
		super.onDestroy();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// handle menu click events
		if (item.getItemId() == R.id.exit) {
			finish();
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.ttt, menu);
		return true;
	}

	/***************************************************************************/
	/********* Networking ******************************************************/
	/***************************************************************************/

	/**
	 * Connect to the server. This method is safe to call from the UI thread.
	 */
	void connect() {

		new AsyncTask<Void, Void, String>() {

			String errorMsg = null;

			@Override
			protected String doInBackground(Void... args) {
				Log.i(TAG, "Connect task started");
				try {
					connected = false;
					socket = new Socket(GROUPCAST_SERVER, GROUPCAST_PORT);
					Log.i(TAG, "Socket created");
					in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
					out = new PrintWriter(socket.getOutputStream());

					connected = true;
					Log.i(TAG, "Input and output streams ready");

				} catch (UnknownHostException e1) {
					errorMsg = e1.getMessage();
				} catch (IOException e1) {
					errorMsg = e1.getMessage();
					try {
						if (out != null)
							out.close();
						if (socket != null)
							socket.close();
					} catch (IOException e) {
					}
				}
				Log.i(TAG, "Connect task finished");
				return errorMsg;
			}

			@Override
			protected void onPostExecute(String errorMsg) {
				if (errorMsg == null) {
					Toast.makeText(getApplicationContext(), "Connected to server", Toast.LENGTH_SHORT).show();

					hideConnectingText();
					showLoginControls();

					// start receiving
					receive();

				} else {
					Toast.makeText(getApplicationContext(), "Error: " + errorMsg, Toast.LENGTH_SHORT).show();
					// can't connect: close the activity
					finish();
				}
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	/**
	 * Start receiving one-line messages over the TCP connection. Received lines
	 * are handled in the onProgressUpdate method which runs on the UI thread.
	 * This method is automatically called after a connection has been
	 * established.
	 */

	void receive() {
		new AsyncTask<Void, String, Void>() {

			@Override
			protected Void doInBackground(Void... args) {
				Log.i(TAG, "Receive task started");
				try {
					while (connected) {

						String msg = in.readLine();

						if (msg == null) { // other side closed the
											// connection
							break;
						}
						publishProgress(msg);
					}

				} catch (UnknownHostException e1) {
				} catch (IOException e1) {
					Log.i(TAG, "IOException in receive task");
				} finally {
					connected = false;
					try {
						if (out != null)
							out.close();
						if (socket != null)
							socket.close();
					} catch (IOException e) {
					}
				}
				Log.i(TAG, "Receive task finished");
				return null;
			}

			@Override
			protected void onProgressUpdate(String... lines) {
				// the message received from the server is
				// guaranteed to be not null
				String msg = lines[0];

				// TODO: act on messages received from the server
				if (msg.startsWith("+OK,NAME")) {
					hideLoginControls();
					showBoard();

					// confirm successful naming
					userName = msg.substring("+OK,NAME,".length());
					Toast.makeText(getApplicationContext(), "Your name is " + userName, Toast.LENGTH_SHORT).show();
					send("LIST,GROUPS");

					return;
				}

				if (msg.startsWith("+ERROR,NAME")) {
					Toast.makeText(getApplicationContext(), msg.substring("+ERROR,NAME,".length()), Toast.LENGTH_SHORT).show();
					return;
				}

				// [ ... and so on for other kinds of messages]
				if (msg.startsWith("+OK,LIST,GROUPS:")) {

					// if group list is empty, open a new group
					if (msg.length() == "OK,LIST,GROUPS:".length()) {
						// group names after the founder
						groupName = "@" + userName + groupNum;
						send("JOIN," + groupName + ",2");
					} else {
						int groupNameStart = 0; // if there is a group with 1
												// member
						if (msg.contains("(1/2)")) {
							for (int i = 0; i < msg.length() - 1; i++) {
								if (msg.charAt(i) == ',')
									groupNameStart = i + 1;
								if (msg.charAt(i) == '(' && msg.charAt(i + 1) == '1') {
									groupName = msg.substring(groupNameStart, i - 1);
									Toast.makeText(getApplicationContext(), "Your group's name is " + groupName, Toast.LENGTH_SHORT).show();
									send("JOIN," + groupName);
									break;
								}
							}
						} else { // if all of groups are full, open a new group
							groupName = "@" + userName + groupNum;
							send("JOIN," + groupName + ",2");
						}
					}

					return;
				}

				// deal with the situations about JOIN
				if (msg.startsWith("+OK,JOIN,")) {
					if (msg.contains("(1/2)")) {
						Toast.makeText(getApplicationContext(), "Waiting for your opponent...", Toast.LENGTH_SHORT).show();
						holder = true;
					} else {
						// let group founder know game start
						if (holder == false) {
							send("LIST,USERS," + groupName);
							Toast.makeText(getApplicationContext(), "Game start!\nYour opponent first.", Toast.LENGTH_SHORT).show();
						}
					}
					return;
				}

				// if received the msg about user names of the group
				if (msg.startsWith("+OK,LIST,USERS")) {
					int colonPostion = msg.indexOf(':');
					int commaPosition = msg.indexOf(',', colonPostion);
					String name1 = msg.substring(colonPostion + 1, commaPosition);
					String name2 = msg.substring(commaPosition + 1);
					if (userName.compareTo(name1) == 0) {
						opponentName = name2;
					} else {
						opponentName = name1;
					}
					Toast.makeText(getApplicationContext(), "Op name is " + opponentName, Toast.LENGTH_SHORT).show();
					if (holder) {
						enableBoardClick();
					} else {
						Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_SHORT).show();
						send("MSG," + opponentName + ",Game Start");
					}
					return;
				}
				if (msg.startsWith("+MSG,")) {
					if (msg.contains("Game Start")) {
						send("LIST,USERS," + groupName);
						return;
					}

					// deal with the moves of the opponent
					int msgStart = msg.lastIndexOf(',') + 1;
					int x = msg.charAt(msgStart) - '0';
					int y = msg.charAt(msgStart + 2) - '0';
					board[x][y].setText("O");
					enableBoardClick();

					return;
				}
				if (msg.startsWith("+OK,MSG")) {
					return;
				}
				// if we haven't returned yet, tell the user that we have an
				// unhandled message
				Toast.makeText(getApplicationContext(), "Unhandled message: " + msg, Toast.LENGTH_LONG).show();
			}

		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
	}

	/**
	 * Disconnect from the server
	 */
	void disconnect() {
		new Thread() {
			@Override
			public void run() {
				try {
					if (connected) {
						connected = false;
					}

					// make sure that we close the output, not the input
					if (out != null)
						out.close();
					// in some rare cases, out can be null, so we need to close
					// the socket itself
					if (socket != null)
						socket.close();
				} catch (IOException e) {
				}

				Log.i(TAG, "Disconnect task finished");
			}
		}.start();
	}

	/**
	 * Send a one-line message to the server over the TCP connection. This
	 * method is safe to call from the UI thread.
	 * 
	 * @param msg
	 *            The message to be sent.
	 * @return true if sending was successful, false otherwise
	 */
	boolean send(String msg) {
		if (!connected) {
			Log.i(TAG, "can't send: not connected");
			return false;
		}

		new AsyncTask<String, Void, Boolean>() {

			@Override
			protected Boolean doInBackground(String... msg) {
				Log.i(TAG, "sending: " + msg[0]);
				out.println(msg[0]);
				return out.checkError();
			}

			@Override
			protected void onPostExecute(Boolean error) {
				if (!error) {
					Toast.makeText(getApplicationContext(), "Message sent to server", Toast.LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), "Error sending message to server", Toast.LENGTH_SHORT).show();
				}
			}
		}.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, msg);

		return true;
	}

	/***************************************************************************/
	/***** UI related methods **************************************************/
	/***************************************************************************/

	/**
	 * Hide the "connecting" text
	 */
	void hideConnectingText() {
		findViewById(R.id.LinearLayout2).setVisibility(View.GONE);
	}

	/**
	 * Show the "connecting" text
	 */
	void showConnectingText() {
		findViewById(R.id.LinearLayout2).setVisibility(View.VISIBLE);
	}

	/**
	 * Hide the login controls
	 */
	void hideLoginControls() {
		findViewById(R.id.LinearLayout3).setVisibility(View.GONE);
	}

	/**
	 * Show the login controls
	 */
	void showLoginControls() {
		findViewById(R.id.LinearLayout3).setVisibility(View.VISIBLE);
	}

	/**
	 * Hide the tictactoe board
	 */
	void hideBoard() {
		findViewById(R.id.TableLayout1).setVisibility(View.GONE);
	}

	/**
	 * Show the tictactoe board
	 */
	void showBoard() {
		findViewById(R.id.TableLayout1).setVisibility(View.VISIBLE);
	}

	/**
	 * Make the buttons of the tictactoe board clickable if they are not marked
	 * yet
	 */
	void enableBoardClick() {
		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				if ("".equals(board[x][y].getText().toString()))
					board[x][y].setEnabled(true);
	}

	/**
	 * Make the tictactoe board non-clickable
	 */
	void disableBoardClick() {
		for (int x = 0; x < 3; x++)
			for (int y = 0; y < 3; y++)
				board[x][y].setEnabled(false);
	}

}
